import React from "react";
import { Card, Image } from "semantic-ui-react";

const AttendeeCard = props => (
  <Card style={{ display: "inline-block", paddingTop: "10px" }}>
    <Card.Content>
      <Image
        floated="left"
        size="mini"
        src="https://react.semantic-ui.com/images/avatar/small/matthew.png"
      />
      <Card.Header>
        {props.attendee.lastName
          ? props.attendee.name + " " + props.attendee.lastName
          : props.attendee.name}
      </Card.Header>
      <Card.Meta>
        <span className="date">{props.attendee.email}</span>
      </Card.Meta>
    </Card.Content>
  </Card>
);

export default AttendeeCard;
