import React from "react";
import { Card } from "semantic-ui-react";
import AttendeeCard from "../common/components/AttendeeCard";

function AttendeeList(props) {
  return (
    <div style={{ paddingTop: "50px" }}>
      <Card.Group itemsPerRow="5">
        {props.attendees.map(attendee => {
          return <AttendeeCard key={attendee.id} attendee={attendee} />;
        })}
      </Card.Group>
    </div>
  );
}

export default AttendeeList;
