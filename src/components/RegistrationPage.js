import React, { useState } from "react";
import RegistrationForm from "./RegitrationForm";
import axios from "axios";
import { toast } from "react-toastify";
import { trackPromise } from "react-promise-tracker";

const RegistrationPage = props => {
  const localUrl = "https://localhost:44365/api/attendees";
  const liveUrl = "https://meetupregister.azurewebsites.net/api/attendees";

  const [errors, setErrors] = useState({});
  const [btnDisabled, setButtonDisabled] = useState(false);

  const [attendee, setAttendee] = useState({
    name: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    personalId: 0
  });

  function onChangeHandler(event) {
    var updatedAttendee = {
      ...attendee,
      [event.target.name]: event.target.value
    };
    setAttendee(updatedAttendee);
  }

  function formIsValid() {
    const _errors = {};

    if (!attendee.name) _errors.name = "Name is required.";
    if (!attendee.email) _errors.email = "Email is required.";
    if (!attendee.personalId) _errors.personalId = "Personal Id is required.";

    setErrors(_errors);
    return Object.keys(_errors).length === 0;
  }

  function onSubmit(event) {
    event.preventDefault();
    if (!formIsValid()) return;
    setButtonDisabled(true);
    axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
    axios.defaults.headers.post["Content-Type"] =
      "application/json; charset=utf-8";

    attendee.personalId = parseInt(attendee.personalId);

    trackPromise(
      axios
        .post(liveUrl, attendee)
        .then(response => {
          toast.success("You have been successfully registered.");
          setButtonDisabled(false);
        })
        .catch(error => {
          if (error.response.status === 400) {
            toast.error(error.response.data);
            setButtonDisabled(false);
          }
        })
    );
  }

  return (
    <div
      style={{
        paddingTop: "50px"
      }}
    >
      <RegistrationForm
        onChange={onChangeHandler}
        onSubmit={onSubmit}
        errors={errors}
        buttonDisabled={btnDisabled}
      />
    </div>
  );
};

export default RegistrationPage;
