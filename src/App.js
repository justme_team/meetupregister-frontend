import React from "react";
import "./App.css";
import sanalogo from "./resources/logo.png";
import RegistrationPage from "./components/RegistrationPage";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Spinner } from "./common/components/spinner";
import RandomPickerPage from "./components/RandomPickerPage";
import { Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <ToastContainer />
      <img
        style={{
          paddingTop: "10px"
        }}
        src={sanalogo}
        alt="logo"
        width="200"
      />
      <Spinner />
      <Switch>
        <Route path="/" exact component={RegistrationPage} />
        <Route path="/selectwinner" exact component={RandomPickerPage} />
      </Switch>
    </div>
  );
}

export default App;
