import React from "react";
import TextInput from "./TextInput";

function RegistrationForm(props) {
  return (
    <form onSubmit={props.onSubmit}>
      <div>
        <div className="col-auto">
          <label className="sr-only" htmlFor="inlineFormInput">
            Name
          </label>
          <TextInput
            id="name"
            name="name"
            value={props.value}
            onChange={props.onChange}
            placeholder="Name"
            type="text"
            class="form-control mb-2"
            error={props.errors.name}
          />
        </div>
      </div>
      <div>
        <div className="col-auto">
          <label className="sr-only" htmlFor="inlineFormInput">
            Last Name
          </label>
          <TextInput
            id="lastName"
            name="lastName"
            onChange={props.onChange}
            placeholder="Last Name"
            type="text"
            class="form-control mb-2"
          />
        </div>
      </div>
      <div>
        <div className="col-auto">
          <label className="sr-only" htmlFor="inlineFormInput">
            Email Address
          </label>
          <TextInput
            id="email"
            name="email"
            onChange={props.onChange}
            placeholder="Email Address"
            type="email"
            class="form-control mb-2"
            error={props.errors.email}
          />
        </div>
      </div>
      <div>
        <div className="col-auto">
          <label className="sr-only" htmlFor="inlineFormInput">
            Phone Number
          </label>
          <TextInput
            id="phoneNumber"
            name="phoneNumber"
            onChange={props.onChange}
            placeholder="Phone Number"
            type="number"
            class="form-control mb-2"
          />
        </div>
      </div>
      <div>
        <div className="col-auto">
          <label className="sr-only" htmlFor="inlineFormInput">
            Personal Id (e.g, Cédula)
          </label>
          <TextInput
            id="personalid"
            name="personalId"
            onChange={props.onChange}
            placeholder="Personal Id (e.g. Cédula, Passport number)"
            type="number"
            class="form-control mb-2"
            error={props.errors.personalId}
          />
        </div>
      </div>
      <input
        disabled={props.buttonDisabled}
        type="submit"
        value="Register"
        className="btn btn-primary"
      />
    </form>
  );
}

export default RegistrationForm;
