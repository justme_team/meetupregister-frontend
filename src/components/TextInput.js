import React from "react";

function TextInput(props) {
  let wrapperClass = "from-group";
  if (props.error) {
    if (props.error.length > 0) {
      wrapperClass += " has-error";
    }
  }

  return (
    <div className={wrapperClass}>
      <div className="field">
        <input
          id={props.id}
          type={props.type}
          name={props.name}
          value={props.value}
          onChange={props.onChange}
          placeholder={props.placeholder}
          className={props.class}
        />
      </div>
      {props.error && <div className="alert alert-danger">{props.error}</div>}
    </div>
  );
}

export default TextInput;
