import React, { useEffect, useState } from "react";
import axios from "axios";
import { trackPromise } from "react-promise-tracker";
import AttendeeList from "./AttendeeList";
import RandomRoller from "react-random-roller";

function RandomPickerPage() {
  const [attendees, setAttendees] = useState([]);

  //   const localUrl = "https://localhost:44365/api/attendees";
  const liveUrl = "https://meetupregister.azurewebsites.net/api/attendees";

  useEffect(() => {
    trackPromise(
      axios.get(liveUrl).then(response => {
        setAttendees(response.data);
      })
    );
  }, []);

  return (
    <div style={{ paddingTop: "30px" }}>
      <RandomRoller
        className="btn btn-primary"
        duration={10000}
        fps={10}
        list={attendees.map(attendee => {
          return attendee.name + " " + attendee.email;
        })}
      />
      <AttendeeList attendees={attendees} />
    </div>
  );
}

export default RandomPickerPage;
